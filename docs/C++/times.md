# ATLAS Open Data C++ framework for 13 TeV analyses

## About

* **Z boson** (ZBosonAnalysis)
  * 993.55s user 20.04s system 90% cpu 18:44.92 total

* **H->gg** (HyyAnalysis)
   * 84.08s user 5.78s system 95% cpu 1:33.77 total

* H->ZZ
./run.sh  658.76s user 17.76s system 85% cpu 13:12.13 total  
  (ZZDiBosonAnalysis)  

* WZ
  ./run.sh  678.57s user 17.11s system 63% cpu 18:20.84 total
  (WZDiBosonAnalysis)

* TTbar
  ./run.sh  2770.97s user 58.98s system 88% cpu 53:03.66 total
  (TTbarAnalysis)

* W boson
  ./run.sh  3277.89s user 46.79s system 91% cpu 1:00:28.64 total
  (WBosonAnalysis)


* **Note** Version of ROOT >6.14 give problem with the include "TProof.h"
